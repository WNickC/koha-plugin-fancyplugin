[%
    T = {
        plugin_name = "Fancy Plugin",
        configuration = "Configuration",
        color_settings = "Color settings",
        color_label = "Color: ",
        save_configuration = "Save configuration",
        cancel = "Cancel",
        pink = "pink",
        red = "red",
        orange = "orange",
        blue = "blue",
    }
%]
