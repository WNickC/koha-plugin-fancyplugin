[%
    T = {
        plugin_name = "Fancy Plugin",
        configuration = "Configuración",
        color_settings = "configuración de color",
        color_label = "Color: ",
        save_configuration = "Guardar configuración",
        cancel = "Cancelar",
        pink = "rosado",
        red = "rojo",
        orange = "naranja",
        blue = "azul",
    }
%]
